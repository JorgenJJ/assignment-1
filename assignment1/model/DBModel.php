<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct()
    {
      $DB = new PDO('mysql:host=localhost;dbname=assignment1', 'root', '');
      $DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $this->db = $DB;
		}


    public function getBookList()
    {
		$booklist = array();
    $stmt = $this->db->prepare('SELECT * FROM book');
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($result as $row){
      array_push($booklist, new Book($row['title'], $row['author'], $row['description'], $row['id']));
    }

    return $booklist;
    }


    public function getBookById($id)
    {
		$book = null;

    $result = $this->db->prepare('SELECT count(*) FROM book');
    $result->execute();
    $count = $result->fetchColumn();

    if($id>0 AND $id <= $count) {
      $stmt = $this->db->prepare('SELECT * FROM book WHERE id=?');
      $stmt->execute([$id]);
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $book = new Book($result['title'], $result['author'], $result['description'], $result['id']);
      }

    return $book;
    }


    public function addBook($book)
    {
      if($_POST['title'] != "" AND $_POST['author'] != ""){
        $book->id = $this->db->lastInsertId() + 1;
        if($book AND $book->id > 0){

          $stmt = $this->db->prepare("INSERT INTO book (id, title, author, description) VALUES(NULL, ?, ?, ?)");
          $stmt->execute([$book->title, $book->author, $book->description]);
        }
      }
    }


    public function modifyBook($book)
    {
      if($book->id > 0){
        $stmt = $this->db->prepare("UPDATE book SET title=?, author=?, description=? WHERE id=?");
        $stmt->execute([$book->title, $book->author, $book->description, $book->id]);
      }
    }

    public function deleteBook($id)
    {
      if($id > 0){

        $stmt = $this->db->prepare("DELETE FROM book WHERE id=?");
        $stmt->execute([$id]);

        $stmt = $this->db->prepare("ALTER TABLE book AUTO_INCREMENT = 1");
        $stmt->execute();
      }
    }

}

?>
